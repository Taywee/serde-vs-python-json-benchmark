data = '''
{
  "id": 1229,
  "name": "alpha",
  "owner": {
    "id": 1231,
    "name": "beta"
  }
}
'''

import timeit
import json

import svpjb

Repository = svpjb.Repository

def native():
  repository = Repository.from_json(data)
  assert repository.owner.name == 'beta'
  repository.id = 1249
  repository.name = 'delta'
  repository.owner.id = 1237
  repository.owner.name = 'gamma'
  assert repository.to_json()


def python():
  repository = json.loads(data)
  assert repository['owner']['name'] == 'beta'
  repository['id'] = 1249
  repository['name'] = 'delta'
  repository['owner']['id'] = 1237
  repository['owner']['name'] = 'gamma'
  assert json.dumps(repository)

timer = timeit.Timer('native()', globals=globals())

number = timer.autorange()[0]

print('native:')
for seconds in timer.repeat(number=number):
  ips = int(number // seconds)
  print(f'{ips} calls per second')


timer = timeit.Timer('python()', globals=globals())

number = timer.autorange()[0]

print('python:')
for seconds in timer.repeat(number=number):
  ips = int(number // seconds)
  print(f'{ips} calls per second')
