use std::borrow::Cow;

use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;

mod ser {
    use std::borrow::Cow;

    use serde::{Deserialize, Serialize};
    #[derive(Debug, Serialize, Deserialize)]
    pub struct User<'a> {
        pub id: u64,
        pub name: Cow<'a, str>,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct Repository<'a> {
        pub id: u64,
        pub name: Cow<'a, str>,
        pub owner: User<'a>,
    }
}

#[pyclass]
#[derive(Debug)]
struct User {
    #[pyo3(get, set)]
    id: u64,

    #[pyo3(get, set)]
    name: String,
}

#[pyclass]
#[derive(Debug)]
struct Repository {
    #[pyo3(get, set)]
    id: u64,

    #[pyo3(get, set)]
    name: String,

    #[pyo3(get, set)]
    owner: Py<User>,
}

impl<'a> IntoPy<User> for ser::User<'a> {
    fn into_py(self, _py: Python<'_>) -> User {
        User {
            id: self.id,
            name: self.name.into_owned(),
        }
    }
}

impl<'a> IntoPy<PyResult<Repository>> for ser::Repository<'a> {
    fn into_py(self, py: Python<'_>) -> PyResult<Repository> {
        Ok(Repository {
            id: self.id,
            name: self.name.into_owned(),
            owner: Py::new(py, self.owner.into_py(py))?,
        })
    }
}

impl<'a> ser::User<'a> {
    fn from_py(user: &'a User) -> Self {
        Self {
            id: user.id,
            name: (&user.name).into(),
        }
    }

    fn into_owned(self) -> ser::User<'static> {
        let name = self.name.into_owned().into();
        ser::User { id: self.id, name }
    }
}

impl<'a> ser::Repository<'a> {
    fn from_py(repository: &'a Repository, py: Python<'_>) -> Self {
        let owner = repository.owner.borrow(py);
        let owner = ser::User::from_py(&owner).into_owned();
        Self {
            id: repository.id,
            name: (&repository.name).into(),
            owner,
        }
    }
}

#[pymethods]
impl User {
    #[staticmethod]
    fn from_json(py: Python<'_>, json: &str) -> PyResult<Self> {
        let user: ser::User =
            serde_json::from_str(json).map_err(|e| PyValueError::new_err(e.to_string()))?;

        Ok(user.into_py(py))
    }

    fn to_json(&self) -> String {
        let user = ser::User::from_py(self);
        serde_json::to_string(&user).unwrap()
    }
}

#[pymethods]
impl Repository {
    #[staticmethod]
    fn from_json(py: Python<'_>, json: &str) -> PyResult<Self> {
        let repository: ser::Repository =
            serde_json::from_str(json).map_err(|e| PyValueError::new_err(e.to_string()))?;

        repository.into_py(py)
    }

    fn to_json(&self, py: Python<'_>) -> String {
        let repository = ser::Repository::from_py(self, py);
        serde_json::to_string(&repository).unwrap()
    }
}

/// A Python module implemented in Rust.
#[pymodule]
fn svpjb(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Repository>()?;
    m.add_class::<User>()?;
    Ok(())
}
